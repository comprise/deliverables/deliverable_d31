#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# create a synthetic Kaldi data dir from a text corpus

import argparse
import sys
import random

parser = argparse.ArgumentParser(description='Synthetic datadir', usage='%(prog)s datadir < text_file')
parser.add_argument('datadir', metavar="datadir", nargs=1, help='target directory (should already exist)')
args = parser.parse_args()


datadir=args.datadir
utt2spk = open(datadir+"/utt2spk","w")
text = open(datadir+"/text","w")
wav = open(datadir+"/wav.scp","w")

# FIXME: this will not work without TILDE TTS voices installed
# replace this your TTS 
voices = ["echo \"%s\" | LC_ALL=lv_LV.utf8 $VISVARIS/g2p-visvaris $VISVARIS/data/rules_nf8.txt $VISVARIS/data/lv.bin /dev/stdout",
    "flite_tilde_lv_bern -t \"%s\" /dev/stdout",
    "flite_tilde_lv_visv -t \"%s\" /dev/stdout"]

utt_i=0
spk_i=0

for line in sys.stdin:
    line = line.strip()
    cmd = random.choice(voices)
    cmd = (cmd + " | sox -t wav - -r 16k -t wav - |" ) % (line)
    spk_i += 1
    utt_i += 1
    spk_id = "spk%i" % spk_i
    utt_id = "%s-part%i" % (spk_id, utt_i)

    utt2spk.write("%s %s\n" % (utt_id, spk_id))

    text.write("%s %s\n" % (utt_id, line))

    wav.write("%s %s\n" % (utt_id, cmd))

utt2spk.close()
wav.close()
text.close()
